/*
 * 5 analogic sensors to midi controllers
 * 
 * 4 faders pour synthetiseurs
 * 1 fader pour le BPM
 */ 

#include "MIDIUSB.h"

int ctlPins[] = {A0, A1, A2, A3, A4};
int ctlMidi[] = {1, 2, 3, 4, 5};
int ctlVal[5];
int ctlLastVal[5];

void setup() {
  Serial.begin(115200);
}

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void loop() {
  for (int i = 0; i < 5; i++) {
    ctlVal[i] = (int) ( analogRead(ctlPins[i]) / 4 );

    if (ctlVal[i] != ctlLastVal[i]) {
      controlChange(1, ctlMidi[i], ctlVal[i]);
    }

    ctlLastVal[i] = ctlVal[i];
  }
  MidiUSB.flush();
  delay(20);
}
