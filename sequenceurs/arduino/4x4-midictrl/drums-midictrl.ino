/*
    Séquenceur 4x4 Philarmonie des enfants

   PINOUT, de haut en bas, de gauche à droite, vue du dessus

   Colonne 1 A5 A4 A3 A2
   Colonne 2 A1 A0 D2 D3
   Colonne 3 D4 D5 D6 D7
   Colonne 4 D8 D9 D10 D11

   Les pins digitales sont branchées sans résistances, lecture en INPUT_PULLUP

   LEDS
   Colonne 1 D13
   Colonne 2 D12
   Colonne 3 D1
   Colonne 4 D0

*/

#include "MIDIUSB.h"

// LEDS
int leds[] = {13, 12, 1, 0};

//INPUTS REEDS
int col1[] = {A5, A4, A3, A2};
int col2[] = {A1, A0, 2, 3};
int col3[] = {4, 5, 6, 7};
int col4[] = {8, 9, 10, 11};

int actualCol[] = {0, 0, 0, 0};

int stepCount = 0;

int midiCtrl[] = {100, 101, 102, 103};

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void setup() {
  Serial.begin(115200);

  //LEDS
  for (int i = 0; i < 4; i++)
  {
    pinMode(leds[i], OUTPUT);
    digitalWrite(leds[i], LOW);
  }

  //REEDS
  //Col1
  for (int i = 0; i < 4; i++)
  {
    pinMode(col1[i], INPUT);
  }
  //Col2
  // !! 2 pins analo
  pinMode(col2[0], INPUT);
  pinMode(col2[1], INPUT);
  // 2 pins digitales
  pinMode(col2[2], INPUT_PULLUP);
  pinMode(col2[3], INPUT_PULLUP);

  //Col3
  for (int i = 0; i < 4; i++)
  {
    pinMode(col3[i], INPUT_PULLUP);
  }

  //Col4
  for (int i = 0; i < 4; i++)
  {
    pinMode(col4[i], INPUT_PULLUP);
  }

  delay(2000);
}

void loop() {
  midiEventPacket_t rx;
  
  do {
    rx = MidiUSB.read();
    
     if (rx.header != 0) {

      // Midi Clock
      if (rx.byte1 == 0xF8) {

        //LEDS
        changeLed(stepCount);

        //REEDS
        checkColumn(stepCount);

        // Play Notes
        for (int i = 0; i < 4; i++)
        {
          if (actualCol[i] == 1)
          {
            controlChange(2, midiCtrl[i], 127);
            MidiUSB.flush();
           }
        }
        stepCount = (stepCount + 1 ) % 4;
      }
    }
  } while (rx.header != 0);

  delay(20);
}

void changeLed(int _stepCount)
{
  for (int i = 0; i < 4; i++)
  {
    if (_stepCount % 4 == i)
    {
      digitalWrite(leds[i], HIGH);
    } else {
      digitalWrite(leds[i], LOW);
    }
  }
}

void checkColumn(int _stepCount)
{
  int col = _stepCount % 4;
  switch (col)
  {
    case 0:
      for (int i = 0; i < 4; i++)
      {
        actualCol[i] = digitalRead(col1[i]);
      }
      break;

    case 1:
      actualCol[0] = digitalRead(col2[0]);
      actualCol[1] = digitalRead(col2[1]);
      // Because of INPUT_PULLUP, nothing means 1, something means 0
      actualCol[2] = !digitalRead(col2[2]);
      actualCol[3] = !digitalRead(col2[3]);
      break;

    case 2:
      for (int i = 0; i < 4; i++)
      {
        actualCol[i] = !digitalRead(col3[i]);
      }
      break;

    case 3:
      for (int i = 0; i < 4; i++)
      {
        actualCol[i] = !digitalRead(col4[i]);
      }
      break;
  }
}
