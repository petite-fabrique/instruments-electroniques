/*
   Séquenceur 4x4 Philarmonie des enfants

   PINOUT, de haut en bas, de gauche à droite, vue du dessus
   Colonne 1 A5 A4 A3 A2
   Colonne 2 A1 A0 D2 D3
   Colonne 3 D4 D5 D6 D7
   Colonne 4 D8 D9 D10 D11

   Les pins digitales sont branchées sans résistances, lecture en INPUT_PULLUP
   LEDS
   Colonne 1 D13
   Colonne 2 D12
   Colonne 3 D1
   Colonne 4 D0
*/

// Leds
int ledPins[] = {13, 12, 1, 0};

// Reed sensors
int reedPins [4][4] = {
  {A5, A4, A3, A2},
  {A1, A0, 2, 3},
  {4, 5, 6, 7}, 
  {8, 9, 10, 11}
};

int col = 0;

void setup() {
  Serial.begin(115200);
  
 
  // Leds setup
  for (int i = 0; i < 4; i++)
  {
    pinMode(ledPins[i], OUTPUT);
    digitalWrite(ledPins[i], LOW);
  }

  
  // Reeds setup - Analog inputs
  pinMode(reedPins[0][0], INPUT);
  pinMode(reedPins[0][1], INPUT);
  pinMode(reedPins[0][2], INPUT);
  pinMode(reedPins[0][3], INPUT);
  
  pinMode(reedPins[1][0], INPUT);
  pinMode(reedPins[1][1], INPUT);
  // Reeds setup - Digital inputs
  pinMode(reedPins[1][2], INPUT_PULLUP);
  pinMode(reedPins[1][3], INPUT_PULLUP);
  
  pinMode(reedPins[2][0], INPUT_PULLUP);
  pinMode(reedPins[2][1], INPUT_PULLUP);
  pinMode(reedPins[2][2], INPUT_PULLUP);
  pinMode(reedPins[2][3], INPUT_PULLUP);

  pinMode(reedPins[3][0], INPUT_PULLUP);
  pinMode(reedPins[3][1], INPUT_PULLUP);
  pinMode(reedPins[3][2], INPUT_PULLUP);
  pinMode(reedPins[3][3], INPUT_PULLUP);

  /*
  for (int i = 0; i < 8; i++) // 2 last lines
  {
    int line = i % 4;
    pinMode(reedPins[ line + 2 ][i], INPUT_PULLUP);
  }
*/
  while (!Serial);
}

void loop() {

  if (Serial.available()) {
    byte inByte = Serial.read();
    if ( inByte == 255) { // Synchro beat
      setLeds();
      readSensors();
      col = (col+1) % 4; // Column number
      //Serial.write(col);
    }
  }
 
  delay(30); // Stability
}

void readSensors()
{
  switch (col)
  {
    case 0:
      for (int i = 0; i < 4; i++)
      {
        if (digitalRead(reedPins[col][i])) Serial.write(i);
      }
      break;

    case 1:
      for (int i = 0; i < 2; i++)
      {
        if (digitalRead(reedPins[col][i])) Serial.write(i);
      }

      for (int i = 2; i < 4; i++)
      {
        if (!digitalRead(reedPins[col][i])) Serial.write(i);
      }
      // Because of INPUT_PULLUP, nothing means 1, something means 0
      break;

    case 2:
      for (int i = 0; i < 4; i++)
      {
        if (!digitalRead(reedPins[col][i])) Serial.write(i);
      }
      break;

    case 3:
      for (int i = 0; i < 4; i++)
      {
        if (!digitalRead(reedPins[col][i])) Serial.write(i);
      }
      break;
  }
}

void setLeds() 
{
  digitalWrite(ledPins[(col - 1) % 4], LOW);
  digitalWrite(ledPins[col], HIGH);
}
