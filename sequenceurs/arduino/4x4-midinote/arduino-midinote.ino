/*
   Basé sur:

   MIDIUSB_clock.ino

   Simple example of beat clock based on MIDI pulse messages
   received from software.

   Tested on Leonardo with Ableton.

   In preferences go to MIDI Sync. Select device Output
   and toggle Sync button, change clock type to Pattern.
   Usually changing Sync Delay is required.

   Created: 19/12/2016
   Author: Ernest Warzocha

   ////////////////////////////////////
   Séquenceur 4x4 Philarmonie des enfants
   _ G. Bertrand, 3615SENOR, 04/2019

   PINOUT, de haut en bas, de gauche à droite, vue du dessus

   Colonne 1 A5 A4 A3 A2
   Colonne 2 A1 A0 D2 D3
   Colonne 3 D4 D5 D6 D7
   Colonne 4 D8 D9 D10 D11

   Les pins digitales sont branchées sans résistances, lecture en INPUT_PULLUP

   LEDS
   Colonne 1 D13
   Colonne 2 D12
   Colonne 3 D1
   Colonne 4 D0

*/

#include "MIDIUSB.h"

//Pulse per quarter note. Each beat has 24 pulses.
//Tempo is based on software inner BPM.
//int ppqn = 0;

// LEDS
int leds[] = {13, 12, 1, 0};

//INPUTS REEDS
int col1[] = {A5, A4, A3, A2};
int col2[] = {A1, A0, 2, 3};
int col3[] = {4, 5, 6, 7};
int col4[] = {8, 9, 10, 11};

int actualCol[] = {0, 0, 0, 0};

int stepCount = 0;

int midiNotes[] = {48, 56, 62, 68};

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void setup() {
  Serial.begin(115200);

  //LEDS
  for (int i = 0; i < 4; i++)
  {
    pinMode(leds[i], OUTPUT);
    digitalWrite(leds[i], LOW);
  }

  //REEDS
  //Col1
  for (int i = 0; i < 4; i++)
  {
    pinMode(col1[i], INPUT);
  }
  //Col2
  // !! 2 pins analo
  pinMode(col2[0], INPUT);
  pinMode(col2[1], INPUT);
  // 2 pins digitales
  pinMode(col2[2], INPUT_PULLUP);
  pinMode(col2[3], INPUT_PULLUP);

  //Col3
  for (int i = 0; i < 4; i++)
  {
    pinMode(col3[i], INPUT_PULLUP);
  }

  //Col4
  for (int i = 0; i < 4; i++)
  {
    pinMode(col4[i], INPUT_PULLUP);
  }

}

void loop() {

  midiEventPacket_t rx;

  do {
    rx = MidiUSB.read();
     if (rx.header != 0) {
    //Count pulses and send note
    if (rx.byte1 == 0xF8) { // Clock midi messages
     // ++ppqn;
     
      //if (ppqn == 24) {
      
        //LEDS
        changeLed(stepCount);

        //REEDS
        checkColumn(stepCount);
        
        /*
        Serial.print ("Colonne n°");
        Serial.print ((stepCount%4)+1);
        Serial.print(" ");
        Serial.print (actualCol[0]);
        Serial.print(" ");
        Serial.print (actualCol[1]);
        Serial.print(" ");
        Serial.print (actualCol[2]);
        Serial.print(" ");
        Serial.println (actualCol[3]);
        */

        // Play Notes
        for (int i = 0; i < 4; i++)
        {
          if (actualCol[i] == 1)
          {
            noteOn(1, midiNotes[i], 127);
            MidiUSB.flush();
          }
          else {
            noteOff(1, midiNotes[i], 0);
            MidiUSB.flush();
          }
        }
        //MidiUSB.flush();
       // ppqn = 0;
        stepCount ++;
      //};
    }
   }
  } while (rx.header != 0);

}

void changeLed(int _stepCount)
{
  for (int i = 0; i < 4; i++)
  {
    if (_stepCount % 4 == i)
    {
      digitalWrite(leds[i], HIGH);
    } else {
      digitalWrite(leds[i], LOW);
    }
  }
}

void checkColumn(int _stepCount)
{
  int col = _stepCount % 4;
  switch (col)
  {
    case 0:
      for (int i = 0; i < 4; i++)
      {
        actualCol[i] = digitalRead(col1[i]);
      }
      break;

    case 1:
      actualCol[0] = digitalRead(col2[0]);
      actualCol[1] = digitalRead(col2[1]);
      // Because of INPUT_PULLUP, nothing means 1, something means 0
      actualCol[2] = !digitalRead(col2[2]);
      actualCol[3] = !digitalRead(col2[3]);
      break;

    case 2:
      for (int i = 0; i < 4; i++)
      {
        actualCol[i] = !digitalRead(col3[i]);
      }
      break;

    case 3:
      for (int i = 0; i < 4; i++)
      {
        actualCol[i] = !digitalRead(col4[i]);
      }
      break;
  }
}
