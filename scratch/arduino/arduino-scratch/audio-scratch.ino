/***************************************************
  Two phase quadrature encoder(Incremental)
* ****************************************************
   To determine motor with encode （CW OR CCW）

  @author Dong
  @version  V1.0
  @date  2016-5-26
  All above must be included in any redistribution
* ****************************************************/
#define  A_PHASE 2
#define  B_PHASE 3
unsigned int flag_A = 0;  //Assign a value to the token bit
unsigned int flag_B = 0;  //Assign a value to the token bit
unsigned int pos = 0;
/** *  */

// set pin numbers:
const int buttonPin = 8;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin
// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
int previousButtonState = 0;


void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);      
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT_PULLUP); 
  //
  pinMode(A_PHASE, INPUT);
  pinMode(B_PHASE, INPUT);
  Serial.begin(9600);   //Serial Port Baudrate: 9600
  //Interrupt trigger mode: RISING
  attachInterrupt(digitalPinToInterrupt( A_PHASE), interrupt, RISING); 
  previousButtonState = digitalRead(buttonPin);
}
void loop() {

  Serial.print("enc ");
  Serial.println(pos);
  delay(10);// Direction judgement
  //
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (buttonState == HIGH && previousButtonState == LOW) {     
    // turn LED on:    
    digitalWrite(ledPin, HIGH);  
    Serial.println("btn 1");
  } 
  if (buttonState == LOW && previousButtonState == HIGH) {     
    // turn LED off:    
    digitalWrite(ledPin, LOW);  
    Serial.println("btn 0");
  } 
  previousButtonState = buttonState;

}
void interrupt()// Interrupt function
{ char i;
  i = digitalRead( B_PHASE);
  if (i == 1)
  {
    flag_A += 1;
  }
  else
  {
    flag_B += 1;
  }
  pos = flag_A - flag_B;
}
