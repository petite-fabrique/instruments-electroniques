 /*
  Misc:
  
  I used fscale (http://playground.arduino.cc/Main/Fscale) to logarithmically map
  proximity values to pitch and volume. The defaults are set to maximize
  sensitivity at large distances from the pads. (nonlinear_pitch & nonlinear_volume)
  Only melodic instruments are implemented, no percussion instruments.
  Default is high pitch and high volume when touch pads, but this can be reversed.
  (direction_pitch & direction_volume)
  Default starting instrument is ocarina

  Written by Nathan Tomlin (nathan.a.tomlin@gmail.com) with tons of code
  stolen from Bare Conductive "Midi_Piano.ino"
   
  "Midi_Piano.ino" (Bare Conductive code written by Stefan Dzisiewski-Smith and 
  Peter Krige. Much thievery from Nathan Seidle in this particular sketch. 
  Thanks Nate - we owe you a cold beer!)
   
  This work is licensed under a MIT license https://opensource.org/licenses/MIT
 
  Copyright (c) 2016, Bare Conductive
 
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

*******************************************************************************/

// compiler error handling
#include "Compiler_Errors.h"
#include "Midi_object.h"

// serial rate
#define baudRate 9600

// include the relevant libraries
#include <MPR121.h>
#include <Wire.h>
#include <SoftwareSerial.h>


MIDIEvent e; 
uint8_t channel = 0; // edit to change the channel number

SoftwareSerial mySerial(12, 10); //Soft TX on 10, we don't use RX in this code

//Touch Board pin setup
byte pin_volume[] = {0, 1, 2} ;      // pint to control volume
const int num_electrodes = 3;
int controler_number[] = {102, 103, 104};

byte min_volume[num_electrodes] ;  // lowest volume
byte max_volume[num_electrodes] ;  // highest volume (max is 127)
byte direction_volume[num_electrodes] ;  // 0 = loudest volume with touch, 1 = lowest volume with touch
int nonlinear_volume[num_electrodes] ; // fscale 'curve' param, 0=linear, -10 = max change at large distance

// initialization stuff for proximity

int level_volume[num_electrodes];
int level_volume_old[num_electrodes];
byte volume[num_electrodes] ;
byte volume_old[num_electrodes] ;
int min_level_volume[num_electrodes] ; // dummy start values - updated during running
int max_level_volume[num_electrodes] ; // dummy start values - updated during running

unsigned char lastOutput[num_electrodes];

//VS1053 setup
//byte note = 0; //The MIDI note value to be played
byte resetMIDI = 8; //Tied to VS1053 Reset line
byte ledPin = 13; //MIDI traffic inidicator
byte velocity = 60;  // midi note velocity for turning on and off


void setup(){



  for(int i=0; i<num_electrodes;i++){
    // Theremin setup
    min_volume[i] = 0;  // lowest volume
    max_volume[i] = 127;  // highest volume (max is 127)
    direction_volume[i] = 0;  // 0 = loudest volume with touch, 1 = lowest volume with touch
    nonlinear_volume[i] = -10; // fscale 'curve' param, 0=linear, -10 = max change at large distance
    
    
    // initialization stuff for proximity
    
    level_volume[i] = 0;
    level_volume_old[i] = 0;
    volume[i] = 0;
    volume_old[i] = 0;
    min_level_volume[i] = 1000; // dummy start values - updated during running
    max_level_volume[i] = 0; // dummy start values - updated during running

    lastOutput[i] = 0;
  }

  Serial.begin(baudRate);
  
  // uncomment the line below if you want to see Serial data from the start
  //while (!Serial);
  
  //Setup soft serial for MIDI control
  mySerial.begin(31250);
   
  // 0x5C is the MPR121 I2C address on the Bare Touch Board
  if(!MPR121.begin(0x5C)){ 
    Serial.println("error setting up MPR121");  
    switch(MPR121.getError()){
      case NO_ERROR:
        Serial.println("no error");
        break;  
      case ADDRESS_UNKNOWN:
        Serial.println("incorrect address");
        break;
      case READBACK_FAIL:
        Serial.println("readback failure");
        break;
      case OVERCURRENT_FLAG:
        Serial.println("overcurrent on REXT pin");
        break;      
      case OUT_OF_RANGE:
        Serial.println("electrode out of range");
        break;
      case NOT_INITED:
        Serial.println("not initialised");
        break;
      default:
        Serial.println("unknown error");
        break;      
    }
    while(1);
  }
  
  // pin 4 is the MPR121 interrupt on the Bare Touch Board
  MPR121.setInterruptPin(4);
  // initial data update
  MPR121.updateTouchData();

  //Reset the VS1053
  pinMode(resetMIDI, OUTPUT);
  digitalWrite(resetMIDI, LOW);
  delay(100);
  digitalWrite(resetMIDI, HIGH);
  delay(100);

  MPR121.updateAll();
}

void loop(){
  
   //MPR121.updateAll();
   MPR121.updateFilteredData();
   
   for(int i=0; i<num_electrodes;i++){
     // volume
     level_volume_old[i] = level_volume[i];
     level_volume[i] = MPR121.getFilteredData(pin_volume[i]);     
     if (level_volume[i] != level_volume_old[i]){
       // dynamically setup level max and mins
       if (level_volume[i] > max_level_volume[i]){
         max_level_volume[i] = level_volume[i];
       }
       if (level_volume[i] < min_level_volume[i]){
         min_level_volume[i] = level_volume[i];
       }   
       // set volume
       else {
         if (direction_volume[i] == 0){
           volume[i] = fscale(min_level_volume[i],max_level_volume[i],max_volume[i],min_volume[i],level_volume[i],nonlinear_volume[i]);
         }
         else if (direction_volume[i] == 1){
           volume[i] = fscale(min_level_volume[i],max_level_volume[i],min_volume[i],max_volume[i],level_volume[i],nonlinear_volume[i]);
         }
         if (volume[i] != volume_old[i]){
           volume_old[i] = volume[i];
           Serial.print("Volume");
           Serial.print(i);
           Serial.print("=");
           Serial.println(volume[i]);
       }
     }
   } 

     // output the correctly mapped value from the input
      e.m3 = (unsigned char)constrain(volume[i] , 0, 127);

      if(e.m3!=lastOutput[i]){ // only output a new controller value if it has changed since last time

      lastOutput[i] =e.m3;

      e.type = 0x08;
      e.m1 = uint8_t(0xB << 4 | channel);
      e.m2 = controler_number[i];     // select the correct controller number - you should use numbers
                                                    // between 102 and 119 unless you know what you are doing
      MIDIUSB.write(e);
   }
  }



      
   // loop delay
   delay(50);
}    

//Plays a MIDI note. Doesn't check to see that cmd is greater than 127, or that data values are less than 127
void talkMIDI(byte cmd, byte data1, byte data2) {
  digitalWrite(ledPin, HIGH);
  digitalWrite(ledPin, LOW);
}




float fscale( float originalMin, float originalMax, float newBegin, float newEnd, float inputValue, float curve){

  float OriginalRange = 0;
  float NewRange = 0;
  float zeroRefCurVal = 0;
  float normalizedCurVal = 0;
  float rangedValue = 0;
  boolean invFlag = 0;


  // condition curve parameter
  // limit range

  if (curve > 10) curve = 10;
  if (curve < -10) curve = -10;

  curve = (curve * -.1) ; // - invert and scale - this seems more intuitive - postive numbers give more weight to high end on output
  curve = pow(10, curve); // convert linear scale into lograthimic exponent for other pow function

  /*
   Serial.println(curve * 100, DEC);   // multply by 100 to preserve resolution  
   Serial.println();
   */

  // Check for out of range inputValues
  if (inputValue < originalMin) {
    inputValue = originalMin;
  }
  if (inputValue > originalMax) {
    inputValue = originalMax;
  }

  // Zero Refference the values
  OriginalRange = originalMax - originalMin;

  if (newEnd > newBegin){
    NewRange = newEnd - newBegin;
  }
  else
  {
    NewRange = newBegin - newEnd;
    invFlag = 1;
  }

  zeroRefCurVal = inputValue - originalMin;
  normalizedCurVal  =  zeroRefCurVal / OriginalRange;   // normalize to 0 - 1 float

  /*
  Serial.print(OriginalRange, DEC);  
   Serial.print("   ");  
   Serial.print(NewRange, DEC);  
   Serial.print("   ");  
   Serial.println(zeroRefCurVal, DEC);  
   Serial.println();  
   */

  // Check for originalMin > originalMax  - the math for all other cases i.e. negative numbers seems to work out fine
  if (originalMin > originalMax ) {
    return 0;
  }

  if (invFlag == 0){
    rangedValue =  (pow(normalizedCurVal, curve) * NewRange) + newBegin;

  }
  else     // invert the ranges
  {  
    rangedValue =  newBegin - (pow(normalizedCurVal, curve) * NewRange);
  }

  return rangedValue;
}
