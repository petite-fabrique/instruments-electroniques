#!/bin/sh
# Start Theremin patch

# Wait for midi
amidiauto &

# Start pd
pd -alsamidi -midiindev 1 -open /home/pi/Bureau/Theremin/main.pd &

# Connect audio
amixer cset numid=3 1