# Instruments électroniques


## Theremin
- RPI3 : Raspbian
- Test : sudo aplay /usr/share/sounds/alsa/Front_Center.wav
- Touch board driver. https://github.com/BareConductive/generic-usb-midi-interface
- Firmware Arduino : midi theremin
- https://github.com/BlokasLabs/amidiauto. Pour connecter Pure Data à la touch board de façon automatique.
- Pure Data et externals de la [Malinette pour RPI] (https://framagit.org/malinette/malinette-soft/malinette-soft-rpi) + rjlib
- Démarrage : ~/.config/lxsession/LXDE/autostart
- Lancement : sh /home/pi/Bureau/autostart.sh
- Patch : /home/pi/Bureau/Theremin/main.pd


## Scratch
- RPI3 : image malinette
- RPI DAC+ : /boot/config.txt



## Séquenceurs
- RPI : image malinette modif rc.local, bashrc
- RPI DAC+ : /boot/config.txt, /etc/asound.conf
- Démarrage : /etc/rc.local
- Lancement : sh /home/pi/malinette-soft-rpi-master/start-nogui.sh
- Patch : /home/pi/malinette-soft-rpi-master/malinette-ide/projects/install/main.pd

- Firmware Arduino : 4x4-midictrl, 5ana-midi